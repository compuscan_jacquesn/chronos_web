const extend = require('smart-extend');
const klaw = require('klaw-sync');
const Sugar = require('sugar');

let config;
const nodeEnv = process.env.NODE_ENV;

console.log('Running for environment: ' + nodeEnv);

if (!nodeEnv || nodeEnv === 'development') {
    config = require('./src/config/config');
}
else {
    config = require('./src/config/config.' + nodeEnv);
}

const staticConfig = {
    srcDir :  'src/',
    generate : {
        dir : 'dist/'
    },
    buildModules : [
        '@nuxt/typescript-build'
    ],
    modules : [
        '@wafl/modules',
        '@nuxtjs/auth-next',
        '@nuxtjs/axios'
    ],
    axios : {
        proxy : true
    },
    router : {
        middleware : ['browserMiddleware']
    },
    auth : {
        watchLoggedIn : false,
        localStorage : false,
        rewriteRedirects: false,
        redirect : {
            login : '/',
            home : '/home'
        },
        vuex : {
            namespace : 'auth'
        },
        strategies : {
            local : {
                autoFetchUser : false,
                token : {
                    property : 'token',
                    global : true
                },
                endpoints : {
                    login : {url : '/api/auth/login', method : 'post', propertyName : 'token'},
                    logout : false,
                    user : false
                }
            }
        }
    },
    build : {
        extractCss : true,
        extend(cfg, { isClient }) {
            // Extend only webpack config for client-bundle
            if (isClient) {
                cfg.devtool = 'cheap-source-map';
            }
        },
        vendor : ['babel-polyfill'],
        watch : ['./src/config', './modules']
    },
    loading : false
};

const commonConfig = require('././src/config/config.common');

let mergedConfig = extend.deep({}, staticConfig, config, commonConfig);

const modulesConfig = new Sugar.Array(klaw('./modules', {nodir : true}))
    .map((el) => {
        return require(el.path);
    }).raw;

for (const i in modulesConfig) {
    mergedConfig = extend.deep.concat({}, mergedConfig, modulesConfig[i]);
}

mergedConfig = extend.concat({}, mergedConfig, {
    server : {
        host : '0.0.0.0',
        port : 3000
    }
});

console.log('Following modules will be loaded: ' + mergedConfig.modules);

module.exports = mergedConfig;