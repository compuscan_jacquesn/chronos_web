module.exports = {
    buildModules : ['@nuxt/components'],
    components : [
        { path : '~/components', extensions : ['vue'] }
    ]
};