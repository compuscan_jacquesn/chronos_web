module.exports = {
    modules : ['@nuxtjs/toast'],
    toast : {
        position : 'top-right',
        duration : 2000
    }
};