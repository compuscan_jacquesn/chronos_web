/* eslint-disable import/no-mutable-exports */

import { Toasted } from 'vue-toasted';
import { LoDashStatic } from 'lodash';
import { Auth } from '@nuxtjs/auth-next';
import Vuetify from 'vuetify';
import Axios from '@nuxtjs/axios';
import * as API from '@chronos/api';

interface Chronos {
    // add plugins below
    $toast                      : Toasted;
    $_                          : LoDashStatic;
    $auth                       : Auth;
    $axios                      : Axios;
    $vuetify                    : Vuetify;
    // validate is the $refs.form validate function
    validate                    : Function;
    reset                       : Function;
    // Add API Types
    $LoginApi                   : API.LoginRoutesApi;
    $UserApi                    : API.UserRoutesApi;
    $AuthApi                    : API.AuthRoutesApi;
    $OvertimeApi                : API.OvertimeRoutesApi;
    $EmailResetApi              : API.EmailPasswordResetRoutesApi;
    $PasswordResetApi           : API.PasswordResetRoutesApi;
}

declare module 'vue/types/vue' {
    interface Vue extends Chronos {}
}

declare module 'nuxt-property-decorator' {
    interface Vue extends Chronos {}
}

declare module '@nuxt/types' {
    // nuxtContext.app.$BranchApi inside asyncData, fetch, plugins, middleware, nuxtServerInit
    interface NuxtAppOptions extends Chronos {}
    // nuxtContext.$BranchApi
    interface Context extends Chronos {}
}

declare module 'vuex/types/index' {
    // this.$BranchApi inside Vuex stores
    interface Store<S> extends Chronos {}
}