/* eslint-disable import/no-mutable-exports */

declare module '*.vue' {
    import Vue from 'vue';
    export default Vue;
}