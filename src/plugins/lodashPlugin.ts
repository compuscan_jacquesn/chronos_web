import {Plugin} from '@nuxt/types';
import _ from 'lodash';

const lodashPlugin: Plugin = (context, inject) => {
    inject('_', _);
};

export default lodashPlugin;