import {Plugin} from '@nuxt/types';
import {AxiosError} from 'axios';
import * as API from '@chronos/api';
import {initGeneralAxios, initAuthAxios, initResetAxios} from '~/utils/api';
import {UIStore, AuthStore} from '~/store';

const uiStore = UIStore();
const authStore = AuthStore();

const axiosPlugin: Plugin = ({$auth, $axios, $toast, app, redirect}, inject) => {
    $axios.onError((error: AxiosError) => {
        console.log(error.response);
        if (error.response && error.config) {
            let errorTxt: string = '';
            if (error.config.url !== '/api/consumer/getConsumerId' && !error.config.url?.includes('/api/enquiry/generatePdfReport/')) {
                if (error.response.status === 401) {
                    errorTxt = 'Unauthorized Action. Logging you out.';
                }
                else if (error.response.status === 504) {
                    errorTxt = 'Unexpected Error Occurred';
                }
                else if (error.response.data.description) {
                    errorTxt = error.response.data.description;
                }
                else if (error.response.data.message) {
                    errorTxt = error.response.data.message;
                }
                else if (error.response.data) {
                    errorTxt = error.response.data;
                }

                $toast.error(errorTxt, {duration : 5000});
            }

            if (['400', '404', '504'].some((status: string) => error.message.search(status) > -1)) {
                uiStore.setLoaderState(false);
                return Promise.reject(error);
            }
            else if (['401'].some((status: string) => error.message.search(status) > -1)) {
                uiStore.setLoaderState(false);
                authStore.logout(app.$auth);
                redirect('/');
                return Promise.reject(error);
            }
            else if (error.response) {
                uiStore.setLoaderState(false);
                return Promise.resolve(false);
            }
        }
        return Promise.reject(error);
    });

    $axios.onRequest((config) => {
        uiStore.setLoaderState(true);
    });

    $axios.onResponse((res) => {
        uiStore.setLoaderState(false);
    });

    const proxyURL = '/api';

    // Auth Api Services
    const LoginApiService = new API.LoginRoutesApi(undefined, proxyURL, $axios);
    const AuthApiService = new API.AuthRoutesApi(undefined, proxyURL, $axios);

    // General Api Services
    const UserApiService = new API.UserRoutesApi(undefined, proxyURL, $axios);

    // Overtime API Services
    const OvertimeApiService = new API.OvertimeRoutesApi(undefined, proxyURL, $axios);

    // Reset Api Services
    const EmailResetApiService = new API.EmailPasswordResetRoutesApi(undefined, proxyURL, $axios);
    const PasswordResetApiService = new API.PasswordResetRoutesApi(undefined, proxyURL, $axios);

    // inject services into the nuxt context
    // Auth Injections
    inject('LoginApi', LoginApiService);
    inject('AuthApi', AuthApiService);

    // General Injections
    inject('UserApi', UserApiService);

    // Overtime Injections
    inject('OvertimeApi', OvertimeApiService)

    // Reset Injections
    inject('EmailResetApi', EmailResetApiService);
    inject('PasswordResetApi', PasswordResetApiService);

    // initialize the services which makes it works within the store
    initGeneralAxios(UserApiService);
    initAuthAxios(LoginApiService, AuthApiService);
    initResetAxios(EmailResetApiService, PasswordResetApiService);
};

export default axiosPlugin;