/* eslint-disable import/no-mutable-exports */

const path = require('path');

const scrollTo =  {
    scrollToBottom : function scrollToBottom() {
        this._scrollToId(document.documentElement.scrollHeight);
    },
    scrollToTop : function scrollToTop() {
        this._scrollToId(0);
    },
    scrollToId : function scrollToId(id, padding) {
        const pad = padding || 0;
        const elem = document.getElementById(id);

        const bounds = elem.getBoundingClientRect();

        if (window.scrollY < bounds.y) {
            this._scrollToId(this._getBoundsForTop(elem) + pad);
        }
        else {
            this._scrollToId(this._getBoundsForBottom(elem) + pad);
        }
    },
    _scrollDownwards : function _scrollDownwards(bound) {
        const step = bound / (500 / 15);
        while (window.scrollY < bound) {
            window.scrollBy(0, step);
        }
    },
    _scrollUpwards : function _scrollUpwards(bound) {
        const step = bound / (500 / 15);
        if (bound < 0) {
            bound *= -1;
        }
        while (document.documentElement.scrollTop > bound) {
            window.scrollBy(0, step * -1);
        }
    },
    _getBoundsForTop : function _getBoundsForTop(e) {
        const box = e.getBoundingClientRect();
        const body = document.body;
        const docElement = document.documentElement;
        const scrollTop = window.pageYOffset || docElement.scrollTop || body.scrollTop;
        const clientTop = docElement.clientTop || body.clientTop || 0;
        const top = box.top + (scrollTop - clientTop);
        return Math.round(top);
    },
    _getBoundsForBottom : function _getBoundsForBottom(e) {
        const rect = e.getBoundingClientRect();
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

        return Math.round(rect.top + scrollTop);
    },
    _easeInOut : function _easeInOut(x) {
        return -0.5 * (Math.cos(Math.PI * x) - 1);
    },
    _scrollToId : function _scrollToId(y) {
        const _this = this;

        const scrollY = window.scrollY || document.documentElement.scrollTop;
        const scrollTargetY = y || 0;

        let currentTime = 0;
        const speed = 500;

        const time = Math.max(0.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, 0.8));

        const startScroll = function startScroll() {
            currentTime += 1 / 60;

            const p = currentTime / time;

            if (p < 1) {
                requestAnimationFrame(startScroll);

                window.scrollTo(0, scrollY + (scrollTargetY - scrollY) * _this._easeInOut(p));
            }
        };

        startScroll();
    }
};

export default function(ctx, inject) {
    ctx.$scroll = scrollTo;
    inject('scroll', scrollTo);
}