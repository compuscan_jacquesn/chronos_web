/* eslint-disable import/no-mutable-exports */

const path = require('path');

module.exports = function(moduleOptions) {
    this.addPlugin({
        src : path.resolve(__dirname, 'scrollto.js'),
        moduleOptions
    });

    this.requireModule([
        '@wafl/scrollto'
    ]);
};

module.exports.meta = require('../package.json');