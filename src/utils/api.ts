// Importing from API package
/* eslint-disable import/no-mutable-exports */

import * as API from '@chronos/api';

let $LoginApi                 : API.LoginRoutesApi;
let $UserApi                  : API.UserRoutesApi;
let $AuthApi                  : API.AuthRoutesApi;
let $OvertimeApi              : API.OvertimeRoutesApi;
let $EmailResetApi            : API.EmailPasswordResetRoutesApi;
let $PasswordResetApi         : API.PasswordResetRoutesApi;

export function initGeneralAxios(
    userApi           : API.UserRoutesApi,
) {
    $UserApi          = userApi;
}

export function initResetAxios(
    emailResetApi     : API.EmailPasswordResetRoutesApi,
    passwordResetApi  : API.PasswordResetRoutesApi
) {
    $EmailResetApi    = emailResetApi;
    $PasswordResetApi = passwordResetApi;
}

export function initAuthAxios(
    loginApi   : API.LoginRoutesApi,
    authApi    : API.AuthRoutesApi
) {
    $LoginApi  = loginApi;
    $AuthApi   = authApi;
}

export {
    $LoginApi,
    $UserApi,
    $AuthApi,
    $OvertimeApi,
    $EmailResetApi,
    $PasswordResetApi
};
