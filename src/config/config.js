module.exports = {
    env : {
        secureToken : '020951d4044454256b1fafb98f673ef5985827zaeba8be1cb827c6d676e58700'
    },
    proxy : {
        '/api' : {
            target: process.env.SERVICES_URL || 'http://localhost:8080',
            changeOrigin : true,
            pathRewrite : {
                '^/api' : '/'
            }
        }
    }
};