module.exports = {
    env : {
        secureToken : '090951d4044454256b2hacg98f673ef6985827eajba8be2cb827c6d670e58700',
        secureCookie : true
    },
    proxy : {
        '/api' : {
            target : process.env.SERVICES_URL || 'http://api.bo.mycreditcheck.co.za:8080',
            changeOrigin : true,
            pathRewrite : {
                '^/api' : '/'
            }
        }
    }

};