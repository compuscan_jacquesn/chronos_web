module.exports = {
    env : {
        secureToken : '090951d4044454256b2fafb98f673ef6985827zaeba8be2cb827c6d670e58700'
    },
    proxy : {
        '/api' : {
            target : process.env.SERVICES_URL || 'http://10.100.12.162:8080',
            changeOrigin : true,
            pathRewrite : {
                '^/api' : '/'
            }
        }
    }
};