module.exports = {
    env : {
        vuexCookieName : 'chronos',
        vuexStorageName : 'chronos-storage',
        secureCookie : false
    },
    css : ['~/assets/scss/main.scss'],
    vuetify : {
        customVariables : ['~/assets/scss/variables.scss'],
        treeShake : true,
        theme : require('@wafl/theme_module').experian
    },
    toast : {
        position : 'top-right'
    },
    plugins : [
        {
            src : '~/plugins/axiosPlugin'
        },
        {
            src : '~/plugins/lodashPlugin'
        }
    ]
};