import {Middleware} from '@nuxt/types';
import Bowser from 'bowser';

const COMPAT_MATRIX: any = {
    edge : '>=40',
    chrome : '>=40',
    firefox : '>=45',
    safari : '>=10',
    ios : {
        safari : '>=10'
    },
    android : '>=8'
};

const browserMiddleware: Middleware = ({redirect, req}) => {
    if (req) {
        const UA: any = req.headers['user-agent'];

        if (UA === undefined || UA === null || typeof UA !== 'string' || UA === '') {
            if (process.server) {
                console.error(`Unknown UserAgent: ${UA}`);
            }
            return redirect('/error/unsupportedBrowser?browser=an Unknown Browser');
        }

        const PARSER: any = Bowser.getParser(UA);
        const BROWSER_INFO: any = PARSER.getBrowser();
        if (!PARSER.satisfies(COMPAT_MATRIX)) {
            return redirect(`/error/unsupportedBrowser?browser=${BROWSER_INFO.name}&ver=${BROWSER_INFO.version}`);
        }
    }
};

export default browserMiddleware;
