import { Vue, Component, Prop } from 'nuxt-property-decorator';

@Component
export default class ErrorBox extends Vue {
    @Prop()
    error !: any;

    @Prop()
    title !: string;

    @Prop({
        default : true
    })
    text !: boolean;

    get errorTitle() {
        return this.title ? this.title : `Error ${this.error.statusCode}`;
    }

    leave() {
        if (this.$auth.loggedIn) {
            this.$router.back();
        }
        else if (this.$route.fullPath === '/error/systemDown') {
            this.$router.push('/');
        }
        else {
            this.$router.push('/');
        }
    }
}