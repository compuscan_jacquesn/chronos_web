import {Vue, Component, Watch, Prop, mixins} from 'nuxt-property-decorator';
import {UIStore} from '~/store';
import validations from "~/mixins/validations";

const uiStore = UIStore();

@Component
export default class AdminUserDialog extends mixins(validations) {
    @Prop({
        type : Object
    })
    user!: any

    @Prop({
        type : String
    })
    mode!: string

    adminForm : boolean = true;
    dialog: boolean = false;
    userTypes: any = [
        {
            text : 'Admin',
            value : 'Y'
        },
        {
            text : 'User',
            value : 'N'
        }
    ];

    @Watch('dialogState')
    watchStateChange(value: boolean) {
        this.dialog = value;
    }

    get isLoading() {
        return uiStore.getLoaderState;
    }

    get dialogState() {
        return uiStore.getAdminUserDialogState;
    }

    setDialogState(value: boolean) {
        uiStore.setAdminUserDialogState(value);
    }

    async addNewUser(user: any) {
        try {
            const results = await this.$UserApi.addUser(user);
            if (results) {
                this.$toast.success('New User Added!');
                this.closeDialog();
                this.$root.$emit('updateAdminUsers');
            }
            else {
                this.$toast.error('Could not add new User');
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    async updateUser(user: any) {
        try {
            const results = await this.$UserApi.updateUserDetails(user);
            if (results) {
                this.$toast.success('User Successfully Updated!');
                this.closeDialog();
                this.$root.$emit('updateAdminUsers');
            }
            else {
                this.$toast.error('Could not update User');
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    saveUser() {
        const userData: any = this.$_.cloneDeep(this.user);
        const form : any = this.$refs.adminForm;
        delete userData.action;
        if(form.validate()){
            (userData && userData.userId) ? this.updateUser(userData) : this.addNewUser(userData);
        }
    }

    closeDialog() {
        const form : any = this.$refs.adminForm;
        form.resetValidation();
        this.setDialogState(false);
    }

    mounted() {
        if (this.dialogState) {
            this.setDialogState(false);
        }
    }
}