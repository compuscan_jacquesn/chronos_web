import {Vue, Component, Watch, Prop, mixins} from 'nuxt-property-decorator';
import {UIStore} from '~/store';
import validations from "~/mixins/validations";

const uiStore = UIStore();

@Component
export default class ReportDialog extends mixins(validations, Vue) {

    reportDateStart: any = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    reportDateStartMenu: boolean = false;
    reportDateEnd: any = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    reportDateEndMenu: boolean = false;

    dialog: boolean = false;

    @Watch('dialogState')
    watchStateChange(value: boolean) {
        this.dialog = value;
    }

    get isLoading() {
        return uiStore.getLoaderState;
    }

    get dialogState() {
        return uiStore.getReportDialogState;
    }

    setDialogState(value: boolean) {
        uiStore.setReportDialogState(value);
    }

    clearInputs() {
        this.reportDateStart = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
        this.reportDateStartMenu = false;
        this.reportDateEnd = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
        this.reportDateEndMenu = false;
    }

    async printReport() {
        try {
            const reportForm : any = this.$refs.reportForm;
            if(reportForm.validate()) {
                const overtime: ReportClass = {
                    dateStart: this.reportDateStart,
                    dateEnd: this.reportDateEnd
                }

                const results = await this.$ReportApi.printCompleteReport(overtime);

                if (results) {
                    this.$toast.success('Report Printed');
                    this.clearInputs();
                    this.closeDialog();
                } else {
                    this.$toast.error('Could not print report');
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    closeDialog() {
        const form : any = this.$refs.reportForm;
        form.resetValidation();
        this.clearInputs();
        this.setDialogState(false);
    }

    mounted() {
        if (this.dialogState) {
            this.setDialogState(false);
        }
    }
}