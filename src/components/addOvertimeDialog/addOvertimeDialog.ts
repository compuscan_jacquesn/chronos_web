import {Vue, Component, Watch, Prop, mixins} from 'nuxt-property-decorator';
import {UIStore} from '~/store';
import validations from "~/mixins/validations";
import {OvertimeLoggedClass} from "@chronos/api";

const uiStore = UIStore();

@Component
export default class AddOvertimeDialog extends mixins(validations, Vue) {
    @Prop()
    overtimeItem !: any;

    description: string = "";
    overtimeDateStart: any = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    overtimeDateStartMenu: boolean = false;
    overtimeTimeStartMenu: boolean = false;
    overtimeTimeStart: any = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(11, 5);
    overtimeDateEnd: any = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    overtimeDateEndMenu: boolean = false;
    overtimeTimeEndMenu: boolean = false;
    overtimeTimeEnd: any = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(11, 5);
    isDoubleTime: string = 'N';
    overtimeId: number = -1;

    dialog: boolean = false;

    @Watch('dialogState')
    watchStateChange(value: boolean) {

        if(value) {
            if (this.overtimeItem && this.overtimeItem.description) {
                this.overtimeId = this.overtimeItem.overtimeId;
                this.description = this.overtimeItem.description;
                const overtimeStartDate: any = new Date(this.overtimeItem.overtimeStart);
                const overtimeEndDate: any = new Date(this.overtimeItem.overtimeEnd);
                this.overtimeDateStart = new Date( overtimeStartDate - new Date().getTimezoneOffset() * 60000).toISOString().substr(0, 10);
                this.overtimeTimeStart = new Date( overtimeStartDate - new Date().getTimezoneOffset() * 60000).toISOString().substr(11, 5);
                this.overtimeDateEnd = new Date( overtimeEndDate - new Date().getTimezoneOffset() * 60000).toISOString().substr(0, 10);
                this.overtimeTimeEnd = new Date( overtimeEndDate - new Date().getTimezoneOffset() * 60000).toISOString().substr(11, 5);
                this.isDoubleTime = this.overtimeItem.isDoubleTime;
            }
        }
        this.dialog = value;
    }

    get isLoading() {
        return uiStore.getLoaderState;
    }

    get dialogState() {
        return uiStore.getAddOvertimeDialogState;
    }

    setDialogState(value: boolean) {
        uiStore.setAddOvertimeDialogState(value);
    }

    clearOvertimeInputs() {
        this.description = "";
        this.overtimeDateStart = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
        this.overtimeDateStartMenu = false;
        this.overtimeTimeStartMenu = false;
        this.overtimeTimeStart = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(11, 5);
        this.overtimeDateEnd = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
        this.overtimeDateEndMenu = false;
        this.overtimeTimeEndMenu = false;
        this.overtimeTimeEnd = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(11, 5);
        this.isDoubleTime = 'N';
        this.overtimeId = -1;
    }

    async addOvertime() {
        try {
            const addOvertimeForm : any = this.$refs.addOvertimeForm;
            if(addOvertimeForm.validate()) {
                const overtime: OvertimeLoggedClass = {
                    overtimeId: this.overtimeId,
                    description: this.description,
                    overtimeStart: this.overtimeDateStart + ' ' + this.overtimeTimeStart,
                    overtimeEnd: this.overtimeDateEnd + ' ' + this.overtimeTimeEnd,
                    isDoubleTime: this.isDoubleTime
                }
                let results: any = false;
                if(this.overtimeId > -1) {
                    results = await this.$OvertimeApi.updateOvertime(overtime);
                } else {
                    results = await this.$OvertimeApi.addOvertime(overtime);
                }
                if (results) {
                    this.$toast.success(this.overtimeItem ? 'Overtime Updated' : 'Overtime Added');
                    this.clearOvertimeInputs();
                    this.closeDialog();
                    await this.$root.$emit('updateOvertime');
                } else {
                    this.$toast.error(this.overtimeItem ? 'Could not update Overtime' : 'Could not add Overtime');
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    closeDialog() {
        const form : any = this.$refs.addOvertimeForm;
        form.resetValidation();
        this.clearOvertimeInputs();
        this.setDialogState(false);
    }

    mounted() {
        if (this.dialogState) {
            this.setDialogState(false);
        }
    }
}