import {Vue, Component, Prop, Watch} from 'nuxt-property-decorator';
import {UIStore} from '~/store';

const uiStore = UIStore();
@Component
export default class QuestionDialog extends Vue {
    @Prop()
    question !: string;

    @Prop()
    overtimeItem !: any;

    @Prop()
    questionType !: string;

    dialog: boolean = false;

    @Watch('dialogState')
    watchStateChange(value: boolean) {
        this.dialog = value;
    }

    get isLoading() {
        return uiStore.getLoaderState;
    }

    get dialogState() {
        return uiStore.getQuestionDialogState;
    }

    setDialogState(value: boolean) {
        uiStore.setQuestionDialogState(value);
    }

    async returnSuccess() {
        this.closeDialog();
        if(this.questionType === 'deleteOvertime') {
            await this.deleteOvertime();
        } else if(this.questionType === 'deleteRedeemedOvertime') {
            await this.deleteRedeemedOvertime();
        } else if(this.questionType === 'declineUserOvertime') {
            await this.declineUserOvertime();
        } else if(this.questionType === 'declineUserRedeemedOvertime') {
            await this.declineUserRedeemedOvertime();
        }
    }


    async deleteOvertime() {
        try {
            if (await this.$OvertimeApi.deleteOvertime(this.overtimeItem.overtimeId)) {
                await this.$root.$emit('updateOvertime');
                this.closeDialog();
                this.$toast.success('Overtime Deleted');
            } else {
                this.$toast.error('Could not delete the Overtime');
            }
        } catch (e) {
            console.log(e);
        }
    }

    async deleteRedeemedOvertime() {
        try{
            if (await this.$OvertimeApi.deleteRedeemedOvertime(this.overtimeItem.redeemedId)) {
                await this.$root.$emit('updateOvertime');
                this.closeDialog();
                this.$toast.success('Redeemed Overtime Deleted');
            } else {
                this.$toast.error('Could not delete the Redeemed Overtime');
            }
        } catch(e) {
            console.log(e);
        }
    }

    async declineUserOvertime() {
        try{
            if (await this.$OvertimeApi.declineOvertime(this.overtimeItem.overtimeId)) {
                await this.$root.$emit('updateAdminOvertime');
                this.closeDialog();
                this.$toast.success('Overtime Declined');
            } else {
                this.$toast.error('Could not decline the Overtime');
            }
        } catch(e) {
            console.log(e);
        }
    }

    async declineUserRedeemedOvertime() {
        try{
            if (await this.$OvertimeApi.declineRedeemedOvertime(this.overtimeItem.redeemedId)) {
                await this.$root.$emit('updateAdminOvertime');
                this.closeDialog();
                this.$toast.success('Redeemed Overtime Declined');
            } else {
                this.$toast.error('Could not Declined the Redeemed Overtime');
            }
        } catch(e) {
            console.log(e);
        }
    }

    closeDialog() {
        this.setDialogState(false);
    }

    mounted() {
        if (this.dialogState) {
            this.setDialogState(false);
        }
    }
}