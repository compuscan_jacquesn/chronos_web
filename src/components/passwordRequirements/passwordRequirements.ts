import { Vue, Component, Watch, Prop } from 'nuxt-property-decorator';
import { UIStore } from '~/store';

const uiStore = UIStore();

@Component
export default class PasswordRequirements extends Vue {
    @Prop({
        type : String
    })
    password!: ''

    specialValidate     : boolean = false;
    uppercaseValidate   : boolean = false;
    lengthValidate      : boolean = false;
    numberValidate      : boolean = false;
    canChangePassword   : boolean = false;
    charText            : string = 'Minimum eight characters long';
    numText             : string = 'Have at least one numeric character';
    specialCharText     : string = 'Use at least one special character, no spaces';
    uppercaseText       : string = 'Include one uppercase and one lowercase letter';
    uppercaseCheck      : any = new RegExp('^(?=.*[A-Z])');
    lowercaseCheck      : any = new RegExp('^(?=.*[a-z])');
    specialCheck        : any = new RegExp('^(?=.*[_\\W])');
    noSpaceCheck        : any = new RegExp('^(?=[\\S]+$)');
    rgxNumber           : any = new RegExp('(?=.*[0-9])');

    @Watch('password')
    watchPasswordCheck(value : string) {
        value.length >= 8 ? this.lengthValidate = true : this.lengthValidate = false;
        this.uppercaseCheck.test(value) && this.lowercaseCheck.test(value) ? this.uppercaseValidate = true : this.uppercaseValidate = false;
        this.specialCheck.test(value) && this.noSpaceCheck.test(value) ? this.specialValidate = true : this.specialValidate = false;
        this.rgxNumber.test(value) ? this.numberValidate = true : this.numberValidate = false;
        this.$emit('passcheck', (this.lengthValidate && this.uppercaseValidate && this.specialValidate && this.numberValidate));
    }
}