import { Vue, Component } from 'nuxt-property-decorator';
import {AuthStore, UIStore} from '~/store';

const authStore = AuthStore();
const uiStore = UIStore();

@Component
export default class AccountButton extends Vue {
    loading         : boolean = false;
    loginForm       : boolean = false;
    showPass        : boolean = false;
    userToken       : string = '';

    credentials     : {} = {
        email       : '',
        password    : ''
    };

    get userData() {
        return authStore.getUser;
    }

    async logout() {
        try {
            if (this.$auth.loggedIn) {
                await this.$AuthApi.logout();
                authStore.logout(this.$auth);
            }
            else {
                authStore.logout(this.$auth);
            }
        }
        catch (error) {
            console.error(error);
        }
    }

    openPass() {
        uiStore.setPassDialogState(true);
    }

    layout() {
        return 'login';
    }
}