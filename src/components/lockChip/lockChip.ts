import { Vue, Component, Prop } from 'nuxt-property-decorator';

@Component
export default class LockChip extends Vue {
    chipLabel : string = '';
    chipColor : string = '';
    chipText : string = '';

    @Prop()
    status !: string;

    @Prop({
        default : false
    })
    consumerChip !: boolean;

    get chipClass() {
        if (this.consumerChip) {
            return 'consumer-chip';
        }
        else {
            return 'status-chip';
        }
    }

    getChipLbl(status : string) {
        if (status === 'L') {
            return 'Locked';
        }
        else if (status === 'A') {
            return 'Activated';
        }
        else if (status === 'D') {
            return 'Deactivated';
        }
    }

    getChipTxt(status : string) {
        if (status === 'L') {
            return '#AA5400';
        }
        else if (status === 'A') {
            return '#007A3B';
        }
        else if (status === 'D') {
            return '#CD0026';
        }
    }

    getChipColor(status : string) {
        if (status === 'L') {
            return '#FCEEBA';
        }
        else if (status === 'A') {
            return '#CDF4D2';
        }
        else if (status === 'D') {
            return '#FFDCE2';
        }
    }
}