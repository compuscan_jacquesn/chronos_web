import {Vue, Component, Watch} from 'nuxt-property-decorator';
import {UIStore} from '~/store';

const uiStore = UIStore();

@Component
export default class Sidebar extends Vue {
    drawer : boolean = true;
    mini : boolean = true;
    items : any = [
        {title : 'Home', icon : 'mdi-home', link : '/home', id : 'bo-home-link'},
        {title : 'Admin Dashboard', icon : 'mdi-security', link : '/admin', id : 'bo-admin-link'}
    ];

    get sidebarState() {
        return uiStore.getSidebarState;
    }

    @Watch('mini')
    watchSidebarStateChange(value: boolean) {
        uiStore.setSidebarState(value);
    }

    get sidebarExists() {
        return document.getElementById('bo-sidebar');
    }

    closeSidebar(e : any) {
        this.mini = true;
    }
}