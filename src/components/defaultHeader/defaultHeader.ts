import {Component, mixins, Watch} from 'nuxt-property-decorator';
import validations from '~/mixins/validations';
import {AuthStore} from '~/store';

const authStore = AuthStore();

@Component
export default class defaultHeader extends mixins(validations)  {
    search : string = '';
    searchForm : boolean = true;
    showChipError : boolean = false;
    chipMessage : string = '';
    chipColor : string = '';
    chipClass : boolean = false;

    get user() {
        return authStore.getUser;
    }

    @Watch('searchForm')
    watchSearchForm(newFormValue : boolean) {
        this.showChipError = !newFormValue;
    }

    linkClass(check : string) {
        const currentLink : string = this.$route.fullPath;
        return  (currentLink === check) ? 'magenta--text font-weight-medium' : 'black--text';
    }

    getChipMessages(message : string) {
        this.showChipError = true;
        this.chipMessage = message;
    }

    mounted() {
        this.chipColor = 'transparent';
        this.chipClass = false;
        this.showChipError = false;
    }
}