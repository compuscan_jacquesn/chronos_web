import {Vue, Component, Watch, Prop, mixins} from 'nuxt-property-decorator';
import {UIStore} from '~/store';
import validations from "~/mixins/validations";
import {OvertimeRedeemedClass} from "@chronos/api";

const uiStore = UIStore();

@Component
export default class RedeemOvertimeDialog extends mixins(validations, Vue) {
    @Prop()
    totalOvertimeLeft !: number;

    overtimeDateStart: any = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    overtimeDateStartMenu: boolean = false;
    overtimeTakenMenu: boolean = false;
    overtimeTaken: number = 0.5;

    dialog: boolean = false;

    @Watch('dialogState')
    watchStateChange(value: boolean) {
        this.dialog = value;
    }

    get isLoading() {
        return uiStore.getLoaderState;
    }

    get dialogState() {
        return uiStore.getRedeemOvertimeDialogState;
    }

    setDialogState(value: boolean) {
        uiStore.setRedeemOvertimeDialogState(value);
    }

    clearOvertimeInputs() {
        this.overtimeDateStart = new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000).toISOString().substr(0, 10);
        this.overtimeDateStartMenu = false;
        this.overtimeTakenMenu = false;
        this.overtimeTaken = 0.5;
    }

    async redeemOvertime() {
        try {
            const redeemOvertimeForm : any = this.$refs.redeemOvertimeForm;
            if(redeemOvertimeForm.validate()) {
                const overtime: OvertimeRedeemedClass = {
                    dateTaken: this.overtimeDateStart,
                    overtimeTaken: this.overtimeTaken
                }

                const results = await this.$OvertimeApi.redeemOvertime(overtime);

                if (results) {
                    this.$toast.success('Overtime Redeemed');
                    this.clearOvertimeInputs();
                    await this.$root.$emit('updateOvertime');
                    this.closeDialog();
                } else {
                    this.$toast.error('Could not redeem Overtime');
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    closeDialog() {
        const form : any = this.$refs.redeemOvertimeForm;
        form.resetValidation();
        this.setDialogState(false);
    }

    mounted() {
        if (this.dialogState) {
            this.setDialogState(false);
        }
    }
}