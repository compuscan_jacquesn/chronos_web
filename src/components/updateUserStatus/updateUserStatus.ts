import {Vue, Component, Watch} from 'nuxt-property-decorator';
import {UIStore} from '~/store';

const uiStore = UIStore();
@Component
export default class UpdateUserStatus extends Vue {
    dialog: boolean = false;

    @Watch('dialogState')
    watchStateChange(value: boolean) {
        this.dialog = value;
    }

    get isLoading() {
        return uiStore.getLoaderState;
    }

    get dialogState() {
        return uiStore.getUpdateStatusDialogState;
    }

    setDialogState(value: boolean) {
        uiStore.setUpdateStatusDialogState(value);
    }

    get status() {
        return uiStore.getUpdateStatusDialogData.status;
    }

    get userId() {
        return uiStore.getUpdateStatusDialogData.userId;
    }

    get type() {
        return uiStore.getUpdateStatusDialogData.type;
    }

    get cardTitle() {
        const action : string = this.status === 'Y' ? 'Deactivate' : 'Activate';
        return action;
    }

    get cardTxt() {
        return this.status === 'Y' ? 'Deactivate' : 'Activate';
    }

    get newStatus() {
        if (this.status === 'Y') {
            return 'N';
        }
        else {
            return 'Y';
        }
    }

    async updateStatus() {
        let results : any = null;
        try {
            results = await this.$UserApi.updateUserStatus(this.userId);
            if (results) {
                this.$toast.success('User status updated!');
                this.closeDialog();
                this.$root.$emit('updateUserStatus', {id : this.userId, status : this.newStatus});
                uiStore.setUpdateStatusDialogData({
                    status : this.newStatus,
                    userId : this.userId,
                    type : 'User'
                });
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    closeDialog() {
        this.setDialogState(false);
    }

    mounted() {
        if (this.dialogState) {
            this.setDialogState(false);
        }
    }
}