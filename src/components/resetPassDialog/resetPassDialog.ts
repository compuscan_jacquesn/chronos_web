import {Component, Watch, mixins} from 'nuxt-property-decorator';
import {AuthStore, UIStore} from '~/store';
import validations from '~/mixins/validations';

const uiStore = UIStore();
const authStore = AuthStore();

@Component
export default class ResetPassDialog extends mixins(validations) {
    dialog              : boolean = false;
    resetPassForm       : boolean = true;
    passRequirements    : boolean = false;
    showPass            : boolean = false;
    showConfirm         : boolean = false;
    showOldPass         : boolean = false;
    newPass             : string = '';
    confirmPass         : string = '';
    oldPass             : string = '';

    @Watch('dialogState')
    watchStateChange(value: boolean) {
        this.dialog = value;
    }

    setDialogState(value: boolean) {
        uiStore.setPassDialogState(value);
    }

    get dialogState() {
        return uiStore.getPassDialogState;
    }

    get isLoading() {
        return uiStore.getLoaderState;
    }

    getPassCheck(val : boolean) {
        this.passRequirements = val;
    }

    get userToken() {
        return authStore.getUserToken;
    }

    async resetPassword() {
        const resetForm : any = this.$refs.resetPassForm;
        const resetData : any = {
            token : this.userToken,
            password    : this.newPass
        };

        if (resetForm.validate() && this.passRequirements && this.userToken) {
            try {
                const results : any = await this.$PasswordResetApi.resetPassword(resetData);
                results ? this.$toast.success('Password successfully reset!') : this.$toast.error('Password could not be reset.');
                this.setDialogState(false);
            }
            catch (error) {
                console.error(error);
            }
        }
    }

    closeDialog(){
        const resetForm : any = this.$refs.resetPassForm;
        resetForm.resetValidation();
        this.setDialogState(false);
    }

    mounted() {
        if (this.dialogState) {
            this.setDialogState(false);
        }
    }
}