import Vue from 'vue';
import loader from '~/mixins/loaderInit';
import sidebar from '~/mixins/sidebarInit';
import dialogs from '~/mixins/dialogStatesInit';
import {AuthStore} from '~/store';

const authStore = AuthStore();

export default Vue.extend({
    name : 'Default',
    mixins : [loader, sidebar, dialogs],
    computed : {
        user() {
            return authStore.getUser;
        }
    }
});