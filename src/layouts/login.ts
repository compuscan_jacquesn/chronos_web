import Vue from 'vue';
import loader from '~/mixins/loaderInit';
export default Vue.extend({
    name    : 'login',
    mixins  : [loader]
});