import { Vue, Component } from 'nuxt-property-decorator';
import { UIStore } from '~/store';

const uiStore = UIStore();
@Component
export default class SidebarInit extends Vue {
    get sidebarState() {
        return uiStore.getSidebarState;
    }

    setSidebar(value: boolean) {
        uiStore.setSidebarState(value);
    }

    created() {
        this.setSidebar(true);
    }
}