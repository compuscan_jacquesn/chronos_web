import {Vue} from 'nuxt-property-decorator';
import {isValidIdNumber} from "south-africa-regex";

import {
    isNotEmpty,
    isEmail,
    maxLength,
    minLength,
    isEmpty,
    isNumber,
    min,
    isInt,
    max
} from 'class-validator';

// @ts-ignore
export default Vue.extend({

    methods: {
        isSAID: (v: string) => {
            if(isValidIdNumber(v).error){
                return 'Not a valid South African ID'
            } else {
                return true;
            }
        },
        required: (v: string) => isNotEmpty(v) || 'Required!',
        minLength: (n: number) => (v: string) => isEmpty(v) || minLength(String(v), n) || `Min length ${n}`,
        maxLength: (n: number) => (v: string) => isEmpty(v) || maxLength(String(v), n) || `Max length ${n}`,
        maxLength10: (v: string) => isEmpty(v) || maxLength(String(v), 10) || 'Max length 10',
        maxLength20: (v: string) => isEmpty(v) || maxLength(String(v), 20) || 'Max length 20',
        maxLength30: (v: string) => isEmpty(v) || maxLength(String(v), 30) || 'Max length 30',
        maxLength40: (v: string) => isEmpty(v) || maxLength(String(v), 40) || 'Max length 40',
        maxLength50: (v: string) => isEmpty(v) || maxLength(String(v), 50) || 'Max length 50',
        maxLength100: (v: string) => isEmpty(v) || maxLength(String(v), 100) || 'Max length 100',
        isEmail: (v: string) => isEmpty(v) || isEmail(v) || 'Invalid email',
        minLength8: (v: string) => isEmpty(v) || minLength(String(v), 8) || 'Min length is 8',
        min: (n: number) => (v: number) => isEmpty(v) || min(Number(v), n) || `Cannot be less than ${n}`,
        max: (n: number) => (v: number) => isEmpty(v) || max(Number(v), n) || `Cannot be more than ${n}`,
        isInt: (v: number) => isEmpty(v) || isInt(Number(v)) || 'Invalid number',
        isNumber: (v: number) => isEmpty(v) || isNumber(Number(v)) || 'Invalid number',
        isStrongPassword: (v: string) => new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z0-9])(?=.{8,})').test(v) || 'Weak'
    }
});