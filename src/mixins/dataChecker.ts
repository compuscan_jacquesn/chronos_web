import { Vue, Component } from 'nuxt-property-decorator';

@Component
export default class DataChecker extends Vue {
    dataCheck(list: any, headers: any) {
        const newList: any = this.$_.cloneDeep(list);
        const newHeaders : any = this.$_.cloneDeep(headers);
        this.$_.forEach(newList, (element) => {
            this.$_.forEach(newHeaders, (child) => {
                const valueCheck : boolean = child in element;
                if (!valueCheck) {
                    element[child] = '-';
                    if(this.$route.fullPath === '/admin'){
                        element.action = element.userStatus;
                    }
                }
            });
        });
        return newList;
    }
}