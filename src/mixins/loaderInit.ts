import { Vue, Component } from 'nuxt-property-decorator';
import { UIStore } from '~/store';

const uiStore = UIStore();
@Component
export default class LoaderInit extends Vue {
    get isLoading() {
        return uiStore.getLoaderState;
    }

    setLoading(value: boolean) {
        uiStore.setLoaderState(value);
    }

    mounted() {
        this.setLoading(false);
    }
}