import { Vue, Component } from 'nuxt-property-decorator';
import { UIStore } from '~/store';

const uiStore = UIStore();
@Component
export default class DialogStatesInit extends Vue {
    setDialogStates(value: boolean) {
        uiStore.setAdminUserDialogState(value);
        uiStore.setPassDialogState(value);
        uiStore.setAddOvertimeDialogState(value),
        uiStore.setUpdateStatusDialogState(value);
    }

    mounted() {
        this.setDialogStates(false);
    }
}