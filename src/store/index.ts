/**
 * New modules can be added to the ./modules directory and plugged in here by
 * importing the file below and adding it to the store at the bottom of the file.
 */

import Vue from 'vue';
import Vuex from 'vuex';
import { getModule } from 'vuex-module-decorators';
import { vuexLocal } from '@wafl/vuex_module';

// Import Vuex Modules
import UIModule from './modules/UIModule';
import AuthModule from './modules/AuthModule';

Vue.use(Vuex);

const secureCookie = process.env.secureCookie === 'true';

// Adding modules to TS Store
export const TheStore = new Vuex.Store({
    modules : {
        UIModule,
        AuthModule
    },
    plugins : [
        vuexLocal({secure : secureCookie}).plugin
    ]
});

// Export Store Modules
export const UIStore              = () => getModule(UIModule, TheStore);
export const AuthStore            = () => getModule(AuthModule, TheStore);
