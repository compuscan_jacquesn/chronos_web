import { Module, VuexModule, Mutation } from 'vuex-module-decorators';

@Module({
    name : 'UIModule',
    namespaced : true,
    stateFactory : true
})
export default class UIModule extends VuexModule {
    loading                   : boolean = false;
    sidebarState              : boolean = false;
    adminUserDialogState      : boolean = false;
    addOvertimeDialogState    : boolean = false;
    questionDialogState       : boolean = false;
    redeemOvertimeDialogState : boolean = false;
    passDialogState           : boolean = false;
    reportDialogState         : boolean = false;
    updateStatusDialogState   : boolean = false;
    updateStatusDialogData    : any = {
        status : '',
        userId : 0,
        type : ''
    };

    get getStatusID() : string {
        if (this.updateStatusDialogData.type === 'User') {
            const id : string = this.updateStatusDialogData.status === 'A' ? 'user-de-activate' : 'user-activate';
            return id + '-' + this.updateStatusDialogData.userId;
        }
        else {
            return 'update-consumer-button';
        }
    }

    get getStatusText() : string {
        return this.updateStatusDialogData.status === 'A' ? 'Deactivate' : 'Activate';
    }

    get getStatusColor() : string {
        return this.updateStatusDialogData.status === 'A' ? '#8C001A' : '#007A3B';
    }

    get getStatusClass() : string {
        return this.updateStatusDialogData.status === 'A' ? 'de-activate-btn' : 'activate-btn';
    }

    get getLoaderState(): boolean {
        return this.loading;
    }

    get getSidebarState(): boolean {
        return this.sidebarState;
    }

    get getAdminUserDialogState(): boolean {
        return this.adminUserDialogState;
    }

    get getAddOvertimeDialogState(): boolean {
        return this.addOvertimeDialogState;
    }

    get getQuestionDialogState(): boolean {
        return this.questionDialogState;
    }

    get getReportDialogState(): boolean {
        return this.reportDialogState;
    }

    get getRedeemOvertimeDialogState(): boolean {
        return this.redeemOvertimeDialogState;
    }

    get getPassDialogState(): boolean {
        return this.passDialogState;
    }

    get getUpdateStatusDialogState(): boolean {
        return this.updateStatusDialogState;
    }

    get getUpdateStatusDialogData(): any {
        return this.updateStatusDialogData;
    }

    @Mutation
    setSidebarState(newSidebarState : boolean): void {
        this.sidebarState = newSidebarState;
    }

    @Mutation
    setLoaderState(newLoaderState : boolean): void {
        this.loading = newLoaderState;
    }

    @Mutation
    setAdminUserDialogState(newAdminDialogState : boolean): void {
        this.adminUserDialogState = newAdminDialogState;
    }

    @Mutation
    setAddOvertimeDialogState(newAddOvertimeDialogState : boolean): void {
        this.addOvertimeDialogState = newAddOvertimeDialogState;
    }

    @Mutation
    setQuestionDialogState(newQuestionDialogState : boolean): void {
        this.questionDialogState = newQuestionDialogState;
    }

    @Mutation
    setReportDialogState(newReportDialogState : boolean): void {
        this.reportDialogState = newReportDialogState;
    }

    @Mutation
    setRedeemOvertimeDialogState(newRedeemOvertimeDialogState : boolean): void {
        this.redeemOvertimeDialogState = newRedeemOvertimeDialogState;
    }

    @Mutation
    setPassDialogState(newPassDialogState : boolean): void {
        this.passDialogState = newPassDialogState;
    }

    @Mutation
    setUpdateStatusDialogState(newUpdateStatusDialogState : boolean): void {
        this.updateStatusDialogState = newUpdateStatusDialogState;
    }

    @Mutation
    setAddOvertimeDialog(newUpdateData : any): void {
        this.updateStatusDialogData.status = newUpdateData.status;
        this.updateStatusDialogData.userId = newUpdateData.id;
        this.updateStatusDialogData.type = newUpdateData.type;
    }

    @Mutation
    setUpdateStatusDialogData(newUpdateData : any): void {
        this.updateStatusDialogData.status = newUpdateData.status;
        this.updateStatusDialogData.userId = newUpdateData.id;
    }
}