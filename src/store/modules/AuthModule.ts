import {Module, VuexModule, Action, Mutation} from 'vuex-module-decorators';

@Module({
    name : 'AuthModule',
    namespaced : true,
    stateFactory : true
})
export default class AuthModule extends VuexModule {
    token: string = '';
    user: object = {
        username: '',
        employeeNumber: -1,
        name: '',
        surname: '',
        active: '',
        isAdmin: '',
        passwordChangeDate: '',
        emailToken: ''
    };

    get getUserToken(): string {
        return this.token;
    }

    get getUser(): object {
        return this.user;
    }

    @Mutation
    setUserToken(newToken: string): void {
        this.token = newToken;
    }

    @Mutation
    setUser(newUser: object): void {
        this.user = newUser;
    }

    @Mutation
    clearUser(): void {
        this.user = {};
        this.token = '';
    }

    @Action
    logout(auth: any) {
        if (process.client) {
            const localStoreName = process.env.vuexStorageName;
            if (localStoreName) {
                window.localStorage.removeItem(localStoreName);
            }
            window.localStorage.clear();
        }
        this.context.commit('clearUser');
        auth.reset();
        auth.logout();
        auth.redirect('login');
    }
}