import {Vue, Component, mixins} from 'nuxt-property-decorator';
import dataCheck from "~/mixins/dataChecker";

import {UIStore} from '~/store';

const uiStore = UIStore();

@Component({layout : 'default'})
export default class Admin extends mixins(dataCheck) {
    // Declaring data variables
    search: string = '';
    actionColor: string = '';
    actionText: string = '';
    chosenUser: any = {};
    chosenMode: string = '';
    headers: any = [
        {text : 'Username', value : 'username'},
        {text : 'Employee Number', value : 'employeeNumber'},
        {text : 'First Name', value : 'name'},
        {text : 'Surname', value : 'surname'},
        {text : 'Active', value : 'active'},
        {text : 'Last Login Date', value : 'lastLoginDate'},
        {text : 'Available Overtime', value : 'availableOvertime'},
        {text : 'Action', value : 'action'}
    ];

    userList: any = [];
    user: any = {};
    headerValueList: any = [
        'username',
        'employeeNumber',
        'name',
        'surname',
        'active',
        'lastLoginDate',
        'availableOvertime',
        'userId'
    ]

    async fetch() {
        await this.fetchUsers();
    }

    openAdminDialog(user: any, mode: string) {
        this.chosenUser = user;
        this.chosenMode = mode;
        uiStore.setAdminUserDialogState(true);
    }

    async fetchUsers() {
        try {
            const temp: any = await this.$UserApi.getUsers();
            if (temp.data.length > 0) {
                this.userList = this.dataCheck(temp.data, this.headerValueList);
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    changeUserStatus(id: number, status: string) {
        debugger;
        const changedUserIndex: any = this.$_.findIndex(this.userList, {userId : id});
        this.userList[changedUserIndex].active = status;
    }

    openStatusDialog(tablePayload: any) {
        const payload : any = {
            status : tablePayload.active,
            id : tablePayload.userId,
        };
        uiStore.setUpdateStatusDialogData(payload);
        uiStore.setUpdateStatusDialogState(true);
    }

    userStatus(status: string) {
        return status === 'Y' ? 'Deactivate' : 'Activate';
    }

    statusIcon(status: string) {
        return (status !== 'Y') ? 'mdi-link-variant' : 'mdi-link-variant-off';
    }

    mounted() {
        this.$root.$on('updateAdminUsers', async () => {
            await this.fetchUsers();
        });

        this.$root.$on('updateUserStatus', (payload: any) => {
            this.changeUserStatus(payload.id, payload.status);
        });
    }
}