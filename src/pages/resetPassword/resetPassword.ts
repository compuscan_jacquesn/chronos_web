import { mixins, Component  } from 'nuxt-property-decorator';
import { AuthStore } from '~/store';
import validations from '~/mixins/validations';

const authStore = AuthStore();

@Component({layout : 'login', auth : false})
export default class ResetPassword extends mixins(validations) {
    resetPassForm       : boolean = true;
    passRequirements    : boolean = false;
    showPass            : boolean = false;
    showConfirm         : boolean = false;
    newPass             : string = '';
    confirmPass         : string = '';
    // confirmPassRule     : any = [(v : any) => v == this.newPass || 'Field must match New Password Field',
    //                              (v : any) => !!v || 'Field is required'];

    getPassCheck(val : boolean) {
        this.passRequirements = val;
    }

    async resetPassword() {
        const resetForm : any = this.$refs.resetPassForm;
        const emailToken : any = this.$route.query.reset;
        const resetData : any = {
            emailToken,
            userPassword    : this.newPass
        };

        if (resetForm.validate()) {
            if (this.passRequirements) {
                if (emailToken) {
                    try {
                        const results : any = await this.$EmailResetApi.createEmailPassword(resetData);
                        if (results.data) {
                            this.$toast.success('Password successfully reset!');
                            this.$router.push('/');
                        }
                    }
                    catch (error) {
                        console.error(error);
                    }
                }
            }
        }
    }

    async fetch() {}
}