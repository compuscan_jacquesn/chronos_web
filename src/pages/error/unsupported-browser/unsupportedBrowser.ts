import { Vue, Component } from 'nuxt-property-decorator';

@Component({layout : 'errorLayout'})
export default class unsupportedBrowser extends Vue {
    headers: any = [
        {text : 'Browser', value : 'browserIcon'},
        {text : '', value : 'browser'},
        {text : 'Supported Version(s)', value : 'version'}
    ];

    data: any = [
        {
            link : 'https://www.google.co.za/chrome/',
            browserIcon : '/error/chromeIcon.png',
            browser : 'Chrome',
            version : '40+'
        },
        {
            link : 'https://www.mozilla.org/en-US/firefox/new/',
            browserIcon : '/error/firefoxIcon.png',
            browser : 'Firefox',
            version : '45+'
        },
        {
            link : 'https://www.apple.com/za/safari/',
            browserIcon : '/error/safariIcon.png',
            browser : 'Safari',
            version : '10+'
        },
        {
            link : 'https://www.microsoft.com/en-us/edge',
            browserIcon : '/error/edge.png',
            browser : 'Microsoft Edge',
            version : '79+'
        }
    ]

    get browser() {
        return this.$router.currentRoute.query.browser;
    }

    get version() {
        return this.$router.currentRoute.query.ver;
    }
}