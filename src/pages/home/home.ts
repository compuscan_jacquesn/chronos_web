import {Vue, Component, mixins} from 'nuxt-property-decorator';
import dataCheck from "~/mixins/dataChecker";

import {UIStore} from '~/store';

const uiStore = UIStore();

@Component({layout : 'default'})
export default class Home extends mixins(dataCheck)  {
    // Declaring data variables
    overtimeLoggedHeaders: any = [
        {text : 'Overtime Start', value : 'overtimeStart'},
        {text : 'Overtime End', value : 'overtimeEnd'},
        {text : 'Is Double Time', value : 'isDoubleTime'},
        {text : 'Description', value : 'description'},
        {text : 'Overtime', value : 'overtime'},
        {text : 'Overtime Left', value : 'overtimeLeft'},
        {text : 'Date Expiring', value : 'dateExpiring'},
        {text : 'Status', value : 'status'},
        {text : 'Action', value : 'action'}
    ];

    overtimeRedeemedHeaders: any = [
        {text : 'Date Created', value : 'dateCreated'},
        {text : 'Overtime Taken', value : 'overtimeTaken'},
        {text : 'Status', value : 'status'},
        {text : 'Action', value : 'action'}
    ];

    overtimeLogged: any = [];
    overtimeRedeemed: any = [];
    totalOvertimeLeft: number = 0;
    overtimeLoggedHeaderValueList: any = [
        'overtimeStart',
        'overtimeEnd',
        'isDoubleTime',
        'description',
        'overtime',
        'overtimeLeft',
        'dateExpiring',
        'status',
    ]
    overtimeRedeemedHeaderValueList: any = [
        'overtimeTaken',
        'status',
        'dateCreated'
    ]

    overtimeItem: any = {};

    question: string = "";

    questionType: string = "";

    normalOvertime: boolean = true;

    async fetch() {
        await this.getOvertime();
    }

    async getOvertime() {
        try {
            const temp : any = await this.$OvertimeApi.getOvertime();

            this.overtimeLogged = [];
            this.overtimeRedeemed = [];

            if (temp.data.overtimeLogged.length > 0) {
                this.overtimeLogged = this.dataCheck(temp.data.overtimeLogged, this.overtimeLoggedHeaderValueList);
            }
            if(temp.data.overtimeRedeemed.length > 0) {
                this.overtimeRedeemed = this.dataCheck(temp.data.overtimeRedeemed, this.overtimeRedeemedHeaderValueList);
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    openAddOvertimeDialog(item: any) {
        this.overtimeItem = {}
        if(item) {
            this.overtimeItem = item;
        }
        uiStore.setAddOvertimeDialogState(true);
    }

    openRedeemOvertimeDialog() {
        uiStore.setRedeemOvertimeDialogState(true);
    }

    openQuestionDialog(item: any, question: string, type: string) {
        this.overtimeItem = item;
        this.question = question;
        this.questionType = type

        uiStore.setQuestionDialogState(true);
    }

    sumFieldOvertime() {
        return this.overtimeLogged.reduce((total: number, item: any) => total + (item.status === 'Approved' ? item.overtime : 0 || 0), 0);
    }

    sumFieldOvertimeLeft() {
        this.totalOvertimeLeft = this.overtimeLogged.reduce((total: number, item: any) => total + (item.status === 'Approved' ? item.overtimeLeft : 0 || 0), 0);
        return this.totalOvertimeLeft;
    }

    sumFieldOvertimeTaken() {
        return this.overtimeRedeemed.reduce((total: number, item: any) => total + (item.status === 'Approved' ? item.overtimeTaken : 0 || 0), 0)
    }

    mounted() {
        this.$root.$on('updateOvertime', async () => {
            await this.getOvertime();
        });
    }
}