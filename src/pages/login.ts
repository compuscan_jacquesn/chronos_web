import {Vue, Component, mixins} from 'nuxt-property-decorator';
import {AuthStore} from '~/store';
import validations from "~/mixins/validations";

const authStore = AuthStore();

@Component
export default class Login extends mixins(validations) {
    loading: boolean = false;
    loginForm: boolean = false;
    showPass: boolean = false;
    errorState: boolean = false;
    userToken: string = '';
    userData: any = {
        name: '',
        username: '',
        userStatus: '',
        isAdmin: ''
    };

    credentials: any = {
        email: '',
        password: ''
    };

    login() {
        this.errorState = false;
        const loginForm : any = this.$refs.loginForm;
        if(loginForm.validate()){
            const credentials: any = {data: this.credentials};
            this.$auth.loginWith('local', credentials)
                .then((results) => {
                    if (results) {
                        this.errorState = false;
                        if (results.data.token) {
                            this.userToken = results.data.token;
                            // Setting auth on axios
                            this.$axios.setToken(this.userToken, 'Bearer');
                            // Setting auth on nuxt/auth
                            this.$auth.setUserToken(this.userToken);
                            // Saving results into store
                            authStore.setUserToken(this.userToken);
                        }
                        if (results.data) {
                            this.userData.username = results.data.username;
                            this.userData.name = results.data.name;
                            this.userData.surname = results.data.surname;
                            this.userData.isAdmin = results.data.isAdmin;
                            this.userData.isActive = results.data.isActive;
                            authStore.setUser(this.userData);
                        }
                        this.$toast.success('Login successful!');
                        this.$auth.redirect('home');
                    } else {
                        this.errorState = true;
                    }
                })
        }
    }

    async getSystemDown() {
        const systemCheck: any = await this.$LoginApi.systemDown();
        if (systemCheck.data) {
            this.$router.push('/error/systemDown');
        }
    }

    async mounted() {
        await this.getSystemDown();
    }

    layout() {
        return 'login';
    }
}
