import { mixins, Component } from 'nuxt-property-decorator';
import { AuthStore } from '~/store';
import validations from '~/mixins/validations';

const authStore = AuthStore();

@Component({layout : 'login', auth : false})
export default class ForgotPassword extends mixins(validations) {
    loading              : boolean = false;
    forgotPassForm       : boolean = false;
    email                : string = '';

    async sendPasswordReq() {
        const resetForm : any = this.$refs.forgotPassForm;
        if(resetForm.validate()){
            try {
                const results : any = await this.$EmailResetApi.resetPasswordByEmail({email : this.email});
                if (results.data) {
                    this.$toast.success('Reset Email Password Sent!');
                }
            }
            catch (error) {
                console.error(error);
            }
        }
    }
}