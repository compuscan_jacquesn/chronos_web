import {Vue, Component, mixins} from 'nuxt-property-decorator';
import dataCheck from "~/mixins/dataChecker";

import {UIStore} from '~/store';

const uiStore = UIStore();

@Component({layout : 'default'})
export default class Admin extends mixins(dataCheck) {
    // Declaring data variables
    search: string = '';
    actionColor: string = '';
    actionText: string = '';
    chosenUser: any = {};
    chosenMode: string = '';
    overtimeHeaders: any = [
        {text : 'Username', value : 'username'},
        {text : 'Description', value : 'description'},
        {text : 'Overtime Start', value : 'overtimeStart'},
        {text : 'Overtime End', value : 'overtimeEnd'},
        {text : 'Overtime', value : 'overtime'},
        {text : 'Double Time', value : 'isDoubleTime'},
        {text : 'Action', value : 'action'}
    ];

    overtimeRedeemedHeaders: any = [
        {text : 'Username', value : 'username'},
        {text : 'Date Taken', value : 'dateTaken'},
        {text : 'Overtime Taken', value : 'overtimeTaken'},
        {text : 'Action', value : 'action'}
    ];

    userOvertimeList: any = [];
    userOvertimeRedeemedList: any = [];
    user: any = {};
    overtimeHeaderValueList: any = [
        'username',
        'overtimeStart',
        'overtimeEnd',
        'overtime',
        'description',
        'isDoubleTime',
        'status'
    ]

    overtimeRedeemedHeaderValueList: any = [
        'username',
        'dateTaken',
        'overtimeTaken',
        'status'
    ]

    question: string = "";
    overtimeItem: any = {};
    questionType: string = '';
    isApprove: boolean = false;

    async fetch() {
        await this.fetchOvertime();
    }

    async approveOvertime(overtimeId: number) {
        try {
            if (await this.$OvertimeApi.approveOvertime(overtimeId)) {
                await this.$root.$emit('updateAdminOvertime');
                this.$toast.success('Overtime Approved');
            } else {
                this.$toast.error('Could not approve the Overtime');
            }
        } catch (e) {
            console.log(e);
        }
    }

    async approveRedeemedOvertime(redeemedId: number) {
        try {
            if (await this.$OvertimeApi.approveRedeemedOvertime(redeemedId)) {
                await this.$root.$emit('updateAdminOvertime');
                this.$toast.success('Redeemed Overtime Approved');
            } else {
                this.$toast.error('Could not approve the Redeemed Overtime');
            }
        } catch (e) {
            console.log(e);
        }
    }

    openQuestionDialog(item: any, question: string, type: string) {
        this.overtimeItem = item;
        this.question = question;
        this.questionType = type

        uiStore.setQuestionDialogState(true);
    }

    openReportDialog() {
        uiStore.setReportDialogState(true);
    }

    async fetchOvertime() {
        try {
            const temp: any = await this.$OvertimeApi.getUserOvertime();

            this.userOvertimeList = [];
            this.userOvertimeRedeemedList = [];
            if (temp.data.userOvertime.length > 0) {
                this.userOvertimeList = this.dataCheck(temp.data.userOvertime, this.overtimeHeaderValueList);
            }
            if(temp.data.userRedeemed.length > 0) {
                this.userOvertimeRedeemedList = this.dataCheck(temp.data.userRedeemed, this.overtimeRedeemedHeaderValueList);
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    sumFieldUserOvertime() {
        return this.userOvertimeList.reduce((total: number, item: any) => total + item.overtime || 0, 0);
    }

    sumFieldUserOvertimeTaken() {
        return this.userOvertimeRedeemedList.reduce((total: number, item: any) => total + (item.overtimeTaken || 0), 0)
    }

    mounted() {
        this.$root.$on('updateAdminOvertime', async () => {
            await this.fetchOvertime();
        });
    }
}