FROM docker.compuscan.co.za/docker/node_lts:latest

EXPOSE 3000

WORKDIR /app

COPY .nuxt/ /app/.nuxt
COPY node_modules/ /app/node_modules
COPY modules/ /app/modules
COPY src/ /app/src
COPY .babelrc /app/
COPY nuxt.config.js /app/
COPY package.json /app/

ENTRYPOINT ["yarn", "run", "start"]